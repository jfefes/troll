<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('sheet_info');	///temporary, helps with bugs in composer
		Schema::dropIfExists('sheets');	//temporary, helps with bugs in composer
		Schema::dropIfExists('users');	//temporary, helps with bugs in composer

		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('email_address')->unique();
			$table->string('password');

			$table->string('first_name');
			$table->string('last_name');

			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});

		Schema::create('sheets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('user_name');
			$table->integer('num_credits');   //For loop in form
		});

		Schema::create('sheet_info', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('sheet_id');
			$table->string('ork_name');
			$table->string('real_name');
			$table->string('credit');
			$table->string('park');
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('sheets');
		Schema::dropIfExists('sheet_info');
	}

}
