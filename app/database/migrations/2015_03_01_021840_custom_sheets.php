<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomSheets extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

		if(Schema::hasColumn('credits_per_user', 'sheets')) {
			$table->dropColumn('credits_per_user');
		}

		Schema::table('sheets', function($table) {
			$table->float('credits_per_user');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('sheets', function($table) {
      if(Schema::hasColumn('credits_per_user', 'sheets')) {
        $table->dropColumn('credits_per_user');
      }

    });
	}

}
