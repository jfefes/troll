<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSheetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sheets', function($table) {
			$table->string('sheet_name');
			$table->string('description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sheets', function($table) {
      if(Schema::hasColumn('sheet_name', 'sheets')) {
        $table->dropColumn('sheet_name');
      }

			if(Schema::hasColumn('description', 'sheets')) {
				$table->dropColumn('description');
			}
    });
	}

}
