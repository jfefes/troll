<?php

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		return View::make('password.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		switch ($response = Password::remind(Input::only('email_address'), function($message)
						{
								$message->from('noreply@ge.goldenvale.org', '[Digital Troll] Password Reminder');
					      $message->subject('[Digital Troll] Password Reminder');
						}))
		{
			case Password::INVALID_USER:
				Session::flash('remind-error', Lang::get($response));
				return Redirect::back();

			case Password::REMINDER_SENT:
				Session::flash('remind-status', Lang::get($response));
				return Redirect::back();
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		return View::make('password.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
			'email_address', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				Session::flash('reset-error', Lang::get($response));
				return Redirect::back();

			case Password::PASSWORD_RESET:
				Session::flash('reset-status', 'Your password has been reset. You can now log in.');
				return Redirect::to('/login');
		}
	}

}
