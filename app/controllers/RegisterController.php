<?php

class RegisterController extends BaseController {

  /*
  |--------------------------------------------------------------------------
  | Default Home Controller
  |--------------------------------------------------------------------------
  |
  | You may wish to use controllers instead of, or in addition to, Closure
  | based routes. That's great! Here is an example controller method to
  | get you started. To route to this controller, just add the route:
  |
  |	Route::get('/', 'HomeController@showWelcome');
  |
  */

  public function index()
  {
    return View::make('register');
  }

  public function doRegister()
  {

    $input = Input::all();

    $validator = Validator::make(
        array(
            'username'        => $input['Username'],
            'firstName'       => $input['firstName'],
            'lastName'        => $input['lastName'],
            'emailAddress'    => $input['emailAddress'],
            'password'        => $input['password'],
            'password_confirm'=> $input['password_confirmation'],

            //'Registration Code'         => $input['regCode'],

        ),
        array(
            'username'        => 'required|min:2|unique:users,email_address',
            'firstName'       => 'min:2',
            'lastName'        => 'min:2',
            'emailAddress'    => 'required|min:2|unique:users,email_address|email',
            'password'        => 'required|min:6|same:password_confirm',

            //'Registration Code' => 'required|in:squiggle12',

        )
    );

    $messages = $validator->messages();
    if(count($messages) > 0)
      return View::make("register", array('input' => $input, 'errors' => $messages->all()));

    // hash their password
    $password = Hash::make($input['password']);

    // TODO: actually use the user class for this
    DB::insert('insert into users (username, email_address, password, first_name, last_name) values (?, ?, ?, ?, ?)',
    array($input['Username'], $input['emailAddress'], $password, $input['firstName'], $input['lastName']));


    // redirect to dashboard
    return View::make('login', array('status' => 'Success! Your account has been created. You can now log-in.'));

  }

}
