<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/


	public function reportFeedback(){

		$data = Input::all();


		Mail::send('emails.feedback', $data, function($message)
		{
			$message->from('noreply@goldenvale.org', ' Amtgard Digital Sign ins');
			$message->to('jfefes@gmail.com')->subject('New feedback reported!');
		});

		return Redirect::back();
	}

}
