<?php

class DashboardController extends BaseController {


  public function index()
  {

    $user_id = Auth::id();

    $username = DB::table('users')
      ->where('id', $user_id)
      ->pluck('username');

    $sheets = DB::table('sheets')
      ->where('user_name', $username)
      ->orderBy('id', 'desc')
      ->get();

    $num_sheets = DB::table('sheets')
      ->select('user_name', 'num_credits')
      ->where('user_name', $username)
      ->count();


    return View::make("dashboard", array(
        'username' => $username,
        'sheets' => $sheets,
        'num_sheets' => $num_sheets

    ));

  }

  public function delete(){
    $input = Input::all();


    DB::table('sheets')
      ->where('id', $input['id'])
      ->delete();

    DB::table('sheet_info')
      ->where('sheet_id', $input['id'])
      ->delete();

    return Redirect::back();
  }
}
