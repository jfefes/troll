<?php

class SheetController extends BaseController {


  public function index()
  {
    return View::make('sheets.new');
  }

  public function newCustom()
  {
    return View::make('sheets.new_custom');
  }

  public function deleteSheet(){

    $id = Input::Get('id');

    DB::table('sheets')->where('id', '=', $id)->delete();

    Session::flash('message', 'The sheet has been deleted');
    return Redirect::back();
  }

  public function create(){
    $input = Input::all();

    $validator = Validator::make(
        array(
            'sheetName'       => $input['sheetName'],
            'credits'         => $input['credits'],
        ),
        array(
            'sheetName'        => 'required|min:2',
            'credits'         => 'required|numeric|in:0,1,2,3,4,5,6,7',
        )
    );

    $messages = $validator->messages();

    if(count($messages) > 0)
      return View::make("sheets.new", array('input' => $input, 'errors' => $messages->all()));

    $user_id = Auth::id();

    $username = DB::table('users')
      ->where('id', $user_id)
      ->pluck('username');

    DB::table('sheets')->insert(array(
      'user_name' => $username,
      'sheet_name' => $input['sheetName'],
      'num_credits' => $input['credits'],
      'credits_per_user' => $input['credits']
    ));

    // redirect to dashboard
    Session::flash('sheet-deleted', 'A new sheet has been created!');
    return Redirect::to('/dashboard');
  }

    public function custom(){
      $input = Input::all();


      $validator = Validator::make(
          array(
              'sheetName'       => $input['sheetName'],
              //'class'           => $input['class'],
              'credits'         => $input['credits'],
          ),
          array(
              'sheetName'        => 'required|min:2',
              //'class'            => 'required|numeric|in:0,1,2,3,4,5,6,7',
              'credits'          => 'required'

          )
      );

      $messages = $validator->messages();

      if(count($messages) > 0)
        return View::make("sheets.new_custom", array('input' => $input, 'errors' => $messages->all()));

      $user_id = Auth::id();

      $username = DB::table('users')
        ->where('id', $user_id)
        ->pluck('username');

      DB::table('sheets')->insert(array(
        'user_name' => $username,
        'sheet_name' => $input['sheetName'],
        'num_credits' => 0,
        //'num_credits' => $input['class'],
        'credits_per_user' => $input['credits']
      ));


    // redirect to dashboard
    Session::flash('sheet-deleted', 'A new sheet has been created!');
    return Redirect::to('/dashboard');

  }

  public function addEntry(){
    $id = Input::Get('id');

    $num_credits = DB::table('sheets')
      ->where('id', $id)
      ->pluck('num_credits');

    $name = DB::table('sheets')
      ->where('id', $id)
      ->pluck('sheet_name');

    return View::make('sheets.add', array('num_credits' => $num_credits, 'sheet_name' => $name, 'id' => $id));
  }

  public function insert(){

    $input = Input::all();

    $signature = Input::get('signature');

    $validator = Validator::make(
        array(
            'Character name' => $input['charName'],
            'realName'       => $input['realName'],
            'park'           => $input['parkName'],
            'signature'      => $signature

        ),
        array(
            'Character name' => 'required|min:2',
            'realName'       => 'required|min:2',
            'park'           => 'required|min:2',
            'signature'      => 'accepted',

        )
    );

    $messages = $validator->messages();

    if(count($messages) > 0)
      return View::make("sheets.add", array('input' => $input, 'num_credits' => $input['num_credits'], 'sheet_name' => $input['sheet_name'], 'id' => $input['id'], 'errors' => $messages->all()));

    if($input['num_credits']>0){
      $credits = Input::get('credits');


      foreach($credits as $credit) {
        DB::table('sheet_info')->insert(array(
          'sheet_id' => $input['id'],
          'ork_name' => $input['charName'],
          'real_name' => $input['realName'],
          'credit' => $credit,
          'park' => $input['parkName']
        ));
      }
    }
    else{
      DB::table('sheet_info')->insert(array(
        'sheet_id' => $input['id'],
        'ork_name' => $input['charName'],
        'real_name' => $input['realName'],
        'park' => $input['parkName']
      ));
    }

    return View::make('sheets.add', array('success' => 'Credits entered.', 'num_credits' => $input['num_credits'], 'sheet_name' => $input['sheet_name'], 'id' => $input['id']));
  }


  public function view(){
    $id = Input::Get('id');

    $sheetinfo = DB::table('sheets')
      ->where('id', $id)
      ->get();


    $entries = DB::table('sheet_info')
      ->where('sheet_id', $id)
      ->orderBy('ork_name', 'Asc')
      ->get();

    $entry_count= DB::table('sheet_info')
      ->where('sheet_id', $id)
      ->orderBy('ork_name', 'Asc')
      ->count();


    return View::make('sheets.view', array(
      'sheetinfo' => $sheetinfo,
      'id' => $id,
      'entries' => $entries,
      'entry_count' => $entry_count
    ));
  }

  public function printSheet($id){

    $num_credits = DB::table('sheets')
      ->where('id', $id)
      ->pluck('credits_per_user');

    $name = DB::table('sheets')
      ->where('id', $id)
      ->pluck('sheet_name');


    $entries = DB::table('sheet_info')
      ->where('sheet_id', $id)
      ->orderBy('park', 'Asc')
      ->get();


    return View::make('sheets.print', array(
      'num_credits' => $num_credits,
      'sheet_name' => $name,
      'id' => $id,
      'entries' => $entries
    ));
  }

  public function email(){

    $input = Input::Get();

    $id = Input::Get('id');

    $num_credits = DB::table('sheets')
      ->where('id', $id)
      ->pluck('credits_per_user');

    $name = DB::table('sheets')
      ->where('id', $id)
      ->pluck('sheet_name');

    $entries = DB::table('sheet_info')
      ->where('sheet_id', $id)
      ->orderBy('real_name', 'Asc')
      ->get();

    $user_id = Auth::id();
    $email = DB::table('users')
      ->where('id', $user_id)
      ->pluck('email_address');

    $sheetinfo = DB::table('sheets')
      ->where('id', $id)
      ->get();

    $data = array('sheet_name' => $name);
    $data = array_add($data, 'num_credits', $num_credits);
    $data = array_add($data, 'entries', $entries);


    Mail::send('emails.sheet', $data, function($message) use ($email, $name)
    {
      $message->from('noreply@goldenvale.org', ' Amtgard Digital Sign ins');
      $message->to($email)->subject($name. ' - sign-in sheet');
    });

    $entry_count= DB::table('sheet_info')
      ->where('sheet_id', $id)
      ->orderBy('ork_name', 'Asc')
      ->count();

    return View::make('sheets.view', array(
      'sheetinfo' => $sheetinfo,
      'id' => $id,
      'entries' => $entries,
      'entry_count' => $entry_count,
      'message' => 'Email sent!'
    ));

  }

  public function editEntry(){
    $input = Input::all();

    $credit = Input::get('credit');

    $validator = Validator::make(
        array(
            'Character name' => $input['charName'],
            'realName'       => $input['realName'],
            'park'           => $input['parkName'],
        ),
        array(
            'Character name' => 'required|min:2',
            'realName'       => 'required|min:2',
            'park'           => 'required|min:2',
        )
    );

    $messages = $validator->messages();

    $num_credits = DB::table('sheets')
      ->where('id', $input['sheet_id'])
      ->pluck('num_credits');

    $name = DB::table('sheets')
      ->where('id', $input['sheet_id'])
      ->pluck('sheet_name');


    $entries = DB::table('sheet_info')
      ->where('sheet_id', $input['sheet_id'])
      ->orderBy('ork_name', 'Asc')
      ->get();



    if(count($messages) > 0)
    return View::make('sheets.view', array(
      'num_credits' => $num_credits,
      'sheet_name' => $name,
      'id' => $input['sheet_id'],
      'entries' => $entries,
      'errors' => $messages->all()));


    DB::table('sheet_info')
      ->where('id', $input['id'])
      ->update(array(
        'ork_name' => $input['charName'],
        'real_name' => $input['realName'],
        'credit' => $credit,
        'park' => $input['parkName']
    ));

    return SheetController::reloadView($input['sheet_id'], 'Your changes have been saved.');

  }

  public function deleteEntry(){
    $input = Input::all();


    DB::table('sheet_info')
      ->where('id', $input['id'])
      ->delete();

    return SheetController::reloadView($input['sheet_id'], 'Entry deleted.');
  }

  public function reloadView($id, $message){

    $sheetinfo = DB::table('sheets')
      ->where('id', $id)
      ->get();

    $entries = DB::table('sheet_info')
      ->where('sheet_id', $id)
      ->orderBy('ork_name', 'Asc')
      ->get();

    return View::make('sheets.view', array(
      'sheetinfo' => $sheetinfo,
      'id' => $id,
      'entries' => $entries,
      'message' => $message
    ));
  }
}
