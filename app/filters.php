<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/



View::share('pretty_date', function($date) {
	$date = strtotime($date);
	return date('M. j', $date);
});

/*
* Filter for getting pretty time from DateTime
*/

View::share('pretty_time', function($date) {
	$date = strtotime($date);
	return date('g A', $date);
});

/*
* Get various user information
*/

View::share('user_info', function($data) {
	if(Auth::guest()) {
		return null;
	}

	switch($data) {
		case "email_address":
			return Auth::user()->email_address;
			break;
		case "username":
			return Auth::user()->username;
			break;
	}
});
/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});



Route::filter('admin', function()
{
		if(! Auth::user()->is_admin) {
				return Redirect::guest('login');
		}
});


/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});


/*
* Force SSL
*/
Route::filter('force.ssl', function()
{
	/* don't force SSL in dev environment
	if( ! Request::secure() && ! App::environment('local') && ! App::environment('staging'))
	{
		return Redirect::secure(Request::path());
	}*/
});
