<!doctype html>

<html>

<head>
  <title>@if(isset($title)) {{ $title }}  | @endif Amtgard Digital Sign-In Sheet </title>

  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="/css/bootstrap.css">
  <link rel="stylesheet" href="/css/style.css">
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>




  <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

</head>

<div class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    @if(null !== ($user_info("username")))
    <a href="/dashboard" class="navbar-brand"><span class="glyphicon glyphicon-home"></span> </a>
    @else
      <a href="/" class="navbar-brand"><span class="glyphicon glyphicon-home"></span> </a>
    @endif
  </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/">Home</a></li>
        @if(null !== ($user_info("username")))
          <li><a href="/settings">User Settings</a>  </li>
          <li><a href="/dashboard">Dashboard</a>  </li>
          <li><a href="/logout">Log Out</a>  </li>
        @else
          <li><a href="/register">Register</a></li>
          <li><a href="/login">Log in</a></li>
        @endif
      </ul>
    </div>
</div>

  <body>

@yield('content')
</div>

    <footer>
      <hr>
      <p class="text-center" style="font-size:10px">Website by Ritari of Caradoc Hold(GV)<br>
      <button class="btn btn-default" data-toggle="modal" data-target="#myModal">Issues or feedback?</button>
      </p>


      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Submit to Admin</h4>
            </div>
            <div class="modal-body">
              {{ Form::open(array('url' => '/feedback', 'class'=>'form-horizontal')) }}
              <div class="row">
                <div class="col-sm-6 col-xs-6" style="margin-top:20px;">
                  {{ Form::label('name', 'Your Name:') }}
                  <input class="form-control" type="text" placeholder="Your Name" id="name" name="name" value="{{{ $input['name'] or '' }}}">
                </div>
                <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
                  <label for="park">Your Park: </label>
                  <input class="form-control" type="text" placeholder="Park Name" id="park" name="park" value="{{{ $input['park'] or '' }}}">
                </div>
                <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
                  <label for="park">Your Email (To respond to feedback!): </label>
                  <input class="form-control" type="text" placeholder="Email address" id="email" name="email" value="{{{ $input['email'] or '' }}}">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
                  <label for="Username">Your Message: </label>
                  <textarea name="messageContents" rows="5" cols="40"></textarea>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              {{ Form::submit('Send', array('class' => 'btn btn-primary')) }}
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>

    </footer>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-59916282-1', 'auto');
    ga('send', 'pageview');

  </script>



    <!-- Code written by James Fefes.

                  .  .
                 .|  |.
                 ||  ||
                 \\()//
                 .={}=.
                / /`'\ \
                ` \  / '
                   `'


    These are Tingle's magic words. Do not steal them! -->
  </body>
</html>
