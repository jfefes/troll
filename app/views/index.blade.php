@extends('layouts.base')

@section('content')

<!--
<div class="alert alert-danger">
    Server message: URL reworking in progress. This app is now located at <a href="http://troll.amtgard.io">troll.amtgard.io</a>
</div>
-->


<div class="caption">
  <h2>Digital Sign-In sheets</h2>
  <ul>
    <li>Remove unnecessary paper sheets every week.</li>
    <li>Significantly reduce time at troll.</li>
    <li>Digital back-ups for paper sheets.</li>
  </ul>

  <a class="btn btn-danger" href="/register">Create account</a> &nbsp; &nbsp;
  <a class="btn btn-danger" href="/login">Login</a>
</div>
@stop
