@extends('layouts.base')

<?php $title="Settings" ?>

@section('content')
<div class="container">


  @if (is_array($errors))
      <div class="alert alert-danger">
        <h5>There were errors with this information: </h5>
        <ul>
        @foreach ($errors as $error)
          <li>
            {{{ $error }}}
          </li>
        @endforeach
        </ul>
      </div>
  @endif

  @if(Session::has('error'))
  <div class="alert alert-danger">
    {{ Session::get('error') }}
  </div>
  @endif

  @if(Session::has('user-status'))
  <div class="alert alert-info">
    {{ Session::get('user-status') }}
  </div>
  @endif

  <h4>{{ $username }}, here you can edit your account settings.</h4>

  <br>

<!----------------    Change username -->
  <div class="well">
    {{ Form::open(array('url' => '/settings/username', 'class'=>'')) }}
    <div class="row">
      <div class="col-xs-8 col-sm-4" style="margin-top:20px;">
        {{ Form::label('username', 'Username:') }}
        <input class="form-control" type="text" placeholder="Username" id="username" name="username" value="{{{ $input['username'] or $username }}}">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-8" style="margin-top:20px;">
        {{ Form::submit('Change username', array('class' => 'btn btn-info')) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>


<!----------------    Change email -->
  <div class="well">
    {{ Form::open(array('url' => '/settings/email', 'class'=>'')) }}
    <div class="row">
      <div class="col-xs-8 col-sm-4" style="margin-top:20px;">
        {{ Form::label('email', 'Email address:') }}
        <input class="form-control" type="email" placeholder="Email address" id="email" name="email" value="{{{ $input['email'] or $email }}}">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-8" style="margin-top:20px;">
        {{ Form::submit('Update email address', array('class' => 'btn btn-success')) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>

<!----------------    Change password -->
  <div class="well">
    {{ Form::open(array('url' => '/settings/password', 'class'=>'')) }}
    <div class="row">
      <div class="col-xs-8 col-sm-4" style="margin-top:20px;">
        {{ Form::label('password', 'Current password:') }}
        <input class="form-control" type="password" placeholder="Current password" id="old_password" name="old_password" value="{{{ $input['old_password'] or '' }}}">
      </div>
    </div>

      <div class="row">
        <div class="col-xs-8 col-sm-4" style="margin-top:20px;">
          {{ Form::label('passNew', 'New password:') }}
          <input class="form-control" type="password" placeholder="New password" id="passNew" name="passNew" value="{{{ $input['passNew'] or '' }}}">
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8 col-sm-4" style="margin-top:20px;">
          {{ Form::label('passConfirm', 'Confirm new password:') }}
          <input class="form-control" type="password" placeholder="Confirm new password" id="passConfirm" name="passConfirm" value="{{{ $input['passConfirm'] or '' }}}">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-8" style="margin-top:20px;">
          {{ Form::submit('Change your password', array('class' => 'btn btn-warning')) }}
        </div>
        {{ Form::close() }}
      </div>
    </div>

<!----------------    delete account -->
    <div class="well">
      <button class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete your account</button>


      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Delete your account.</h4>
            </div>
            <div class="modal-body">
              {{ Form::open(array('url' => '/settings/delete', 'class'=>'')) }}
              <p>Are you sure you want to delete your account? This can <strong>NOT</strong> be undone.
              </p>
              <div class="row">
                <div class="col-xs-8" style="margin-top:20px;">
                  {{ Form::label('password', 'Enter your password and press delete:') }}
                  <input class="form-control" type="password" placeholder="Password" id="password" name="password" value="{{{ $input['password'] or '' }}}">
                <hr>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
  </div>



  <!--
  <div class="well">
    Ork access
  </div>
    -->


@stop
