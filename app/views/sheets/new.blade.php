@extends('layouts.base')
<?php $title="New Sheet" ?>

@section('content')
<div class="container">




<div class="row">
  @if (is_array($errors))
      <div class="alert alert-danger">
        <h5>There were errors with this information: </h5>
        <ul>
        @foreach ($errors as $error)
          <li>
            {{{ $error }}}
          </li>
        @endforeach
        </ul>
      </div>
  @endif

  @if(isset($success))
      <div class="alert alert-success">
        {{{ $success }}}
      </div>
  @endif

  <div class="col-xs-11 col-xs-offset-1">
    <div class="alert alert-info text-center">
      Use the form below to create a new sheet. It's that easy.<br> <br>
      Create a custom sheet instead:
      <a href="/sheets/custom" class="btn btn-purple" style="margin-right:10px">Custom sheet (beta)</a>
    </div>
  </div>
</div>


  <div class="well" style="margin-top:15px">
    {{ Form::open(array('url' => '/sheets/create', 'class'=>'form-horizontal')) }}
    <div class="row">
      <div class="col-sm-6 col-xs-8 col-xs-offset-3" style="margin-top:20px;">
        {{ Form::label('sheetName', 'Sheet Name:') }}
        <input class="form-control" type="text" placeholder="Sheet Name" id="sheetName" name="sheetName" value="{{{ $input['sheetName'] or '' }}}">
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-xs-8 col-xs-offset-3" style="margin-top:20px;">
        <label for="credits">Amount of credits: </label>
        <input class="form-control" type="text" placeholder="How many credits on this sheet?" id="credits" name="credits" value="{{{ $input['credits'] or '1' }}}">
      </div>
    </div>

      <!-- TODO: Drescription as textarea -->


    <div class="col-xs-offset-3" style="margin-top:20px;">
      {{ Form::submit('Create New Sheet', array('class' => 'btn btn-primary')) }}
    </div>
    <?php echo Form::token(); ?>
    {{ Form::close() }}
  </div>

@stop
