@extends('layouts.base')

@section('content')
<div class="container">


<div class="row">
  <div class="col-sm-6 col-xs-12">
    <h3>Sign in sheet for {{ $sheet_name }}</h3>
  </div>

  <div class="col-sm-3 col-xs-7">
    <br>
    <a href="/dashboard" class="btn btn-primary">To Dashboard</a>
  </div>

  <div class="visible-xs"><div style="height:20px"></div></div>


  <div class="col-sm-3 col-xs-4">
    <br>
    {{ Form::open(array('url' => '/sheets/view', 'class'=>'form-horizontal')) }}
    <input type="hidden" name="id" value="{{{ $id }}}"/>
    {{ Form::submit('View this sheet', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}
  </div>

</div>


@if (is_array($errors))
    <div class="alert alert-danger">
      <h5>There were errors with this information: </h5>
      <ul>
      @foreach ($errors as $error)
        <li>
          {{{ $error }}}
        </li>
      @endforeach
      </ul>
    </div>
@endif

<br>

@if(isset($signature))
  <div class="alert alert-danger">
    {{ $signature }}
  </div>
@endif

<br>

@if(isset($success))
  <div class="alert alert-success">
    {{ $success }}
  </div>
@endif


<?php $creditArray = array(
  'Classes' => array("Archer" => "Archer", "Assassin" => "Assassin",  "Barbarian" => "Barbarian", "Bard" => "Bard", "Druid" => "Druid", "Healer" => "Healer", "Monk" => "Monk", "Monster" => "Monster", "Scout" => "Scout",  "Warrior" => "Warrior", "Wizard" => "Wizard"),

  'Knight classes' => array("Anti-Paladin" => "Anti-Paladin", "Paladin" => "Paladin"),
  'Other' => array("Color" => "Color", "Undeclared" => "Undeclared", "Peasant" => "Peasant", "Reeve" => "Reeve"));
?>

  <div class="well" style="margin-top:20px">
      {{ Form::open(array('url' => '/entry/add', 'class'=>'form-horizontal')) }}

      {{ Form::hidden('num_credits', $num_credits); }}
      {{ Form::hidden('sheet_name', $sheet_name); }}
      {{ Form::hidden('id', $id); }}

      <div class="row">
        <div class="col-sm-6 col-xs-8" style="margin-top:20px;">
          {{ Form::label('charName', 'Character (ORK) Name:') }}
          <input class="form-control" type="text" placeholder="Character Name" id="charName" name="charName" value="{{{ $input['charName'] or '' }}}">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 col-xs-8" style="margin-top:20px;">
          <label for="lastName">Real Name: </label>
          <input class="form-control" type="text" placeholder="Real Name" id="realName" name="realName" value="{{{ $input['realName'] or '' }}}">
        </div>
      </div>

      <div class="row">


      @if(isset($input))
        <?php $credits = Input::get('credits');
          $i=0; ?>


        @foreach ($credits as $credit)
          <div class="col-sm-2 col-xs-8" style="margin-top:20px;">
            <label for="Username">Credit: #{{ $i+1 }} </label>
            {{ Form::select('credits[]', $creditArray, $credit) }}
          </div>
          <?php $i++; ?>
        @endforeach

      @else
        @for ($i = 0; $i < $num_credits; $i++)
        <div class="col-sm-2 col-xs-8" style="margin-top:20px;">
          <label for="Username">Credit #{{ $i+1 }}: </label>
          {{ Form::select('credits[]', $creditArray) }}
        </div>
        @endfor
      @endif

      </div>

      <div class="row">
        <div class="col-sm-6 col-xs-8" style="margin-top:20px;">
          <label for="parkName">Park Name: </label>
          <input class="form-control" type="text" placeholder="Park Name" id="parkName" name="parkName" value="{{{ $input['parkName'] or '' }}}">
        </div>
      </div>

      <div class="row">

        <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="signature">I agree to an electronic signature: &nbsp;</label>
          {{ Form::checkbox('signature', 'true', false, array('style' => '')) }}
        </div>
      </div>

      <div style="margin-top:20px;">
        {{ Form::submit('Add Credit', array('class' => 'btn btn-primary')) }}
      </div>
    {{ Form::close() }}
  </div>
@stop
