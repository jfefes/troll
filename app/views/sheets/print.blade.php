<html>
<link rel="stylesheet" href="/css/bootstrap.css">

<body>
<div class="container">


  <h4>Sheet name: {{ $sheet_name }}</h4>
  <h5>Credits per player: {{ $num_credits }} </h5>


  <div class="row">
    <div class="col-xs-3">
      <strong>ORK Name:</strong>
    </div>

    <div class="col-xs-3">
      <strong>Real Name:</strong>
    </div>

    <div class="col-xs-3">
      <strong>Park:</strong>
    </div>

    <div class="col-xs-3">
      <strong>Credit:</strong>
    </div>
  </div>

  @foreach ($entries as $entry)

  <div class="row">
    <div class="col-xs-3">
      {{ $entry->ork_name}}
    </div>

    <div class="col-xs-3">
      {{ $entry->real_name }}
    </div>

    <div class="col-xs-3">
      {{ $entry->park }}
    </div>

    <div class="col-xs-3">
      {{ $entry->credit }}
    </div>

  </div>
  @endforeach

</body>
</html>
