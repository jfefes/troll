@extends('layouts.base')

<?php $creditArray = array(
  'Classes' => array("Archer" => "Archer", "Assassin" => "Assassin",  "Barbarian" => "Barbarian", "Bard" => "Bard", "Druid" => "Druid", "Healer" => "Healer", "Monk" => "Monk", "Monster" => "Monster", "Scout" => "Scout",  "Warrior" => "Warrior", "Wizard" => "Wizard"),

  'Knight classes' => array("Anti-Paladin" => "Anti-Paladin", "Paladin" => "Paladin"),
  'Other' => array("Color" => "Color", "Undeclared" => "Undeclared", "Reeve" => "Reeve", "Peasant" => "Peasant"));
?>

@section('content')
<div class="container">

@foreach($sheetinfo as $sheet)

  <h3>Sheet name: {{ $sheet->sheet_name }}</h3>
  <h5>Credits per player:
    @if($sheet->credits_per_user > $sheet->num_credits)
      {{ $sheet->credits_per_user }}
    @else
      {{ $sheet->num_credits }}
    @endif
    <br>
    Unique Entries: {{ $entry_count = $entry_count/$sheet->credits_per_user }}
    <br>
    External id: {{ $id }}
  </h5>

  @if(isset($success))
    <div class="alert alert-success">
      {{ $success }}
    </div>
  @endif


  @if(isset($message))
    <div class="alert alert-info">
      {{ $message }}
    </div>
  @endif

  @if(empty($entries))
    <div class="well text-center">
      This sheet is empty...click to get started. &nbsp;
      {{ Form::open(array('url' => '/sheets/addEntry', 'class'=>'form-horizontal')) }}
      <input type="hidden" name="id" value="{{{ $id }}}"/>
      {{ Form::submit('Add new Entries', array('class' => 'btn btn-primary')) }}
      {{ Form::close() }}
    </div>
  @else

  <div class="well">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-6">
        <a href="/dashboard" class="btn btn-default">To Dashboard</a>
      </div>

      <div class="col-md-3 col-sm-3 col-xs-6">
        {{ Form::open(array('url' => '/sheets/addEntry', 'class'=>'form-horizontal')) }}
        <input type="hidden" name="id" value="{{{ $id }}}"/>
        {{ Form::submit('Add new Entries', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
      </div>

      <div class="visible-xs"><div style="height:55px"></div></div>

      <div class="col-md-4 col-md-push-1 col-sm-3 col-xs-6">
        <a href="/sheets/print/{{{ $id }}}" class="btn btn-warning">Printer Friendly</a>
      </div>

      <div class="col-md-2 col-sm-3 col-xs-6">
        {{ Form::open(array('url' => '/entries/email', 'class'=>'form-horizontal')) }}
        <input type="hidden" name="id" value="{{{ $id }}}"/>
        {{ Form::submit('Email this sheet', array('class' => 'btn btn-success')) }}
        {{ Form::close() }}
      </div>
    </div>
  </div>

  @foreach ($entries as $entry)
    <div class="well" style="padding:5px">
      <div class="row" style="margin:20px">

        <div class="row">
          <div class="col-sm-4 col-sm-offset-2 col-xs-6">
            <strong>ORK Name:</strong> {{ $entry->ork_name}}
          </div>

          <div class="col-sm-6 col-xs-6">
            <strong>Real Name:</strong> {{ $entry->real_name }}
          </div>
        </div>

        <div class="row" style="margin-top:20px">
          <div class="col-sm-4 col-sm-offset-2 col-xs-6">
            <strong>Credit:</strong> {{ $entry->credit }}
          </div>

          <div class="col-sm-6 col-xs-6">
            <strong>Park:</strong> {{ $entry->park }}
          </div>
        </div>

        <div class="row" style="margin-top:20px">

          <div class="col-sm-4 col-sm-offset-2 col-xs-6">
            <button class="btn btn-info" data-toggle="modal" data-target="#editModal{{$entry->id}}">Edit this Entry</button>
          </div>

          <div class="col-sm-4 col-xs-6">
            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$entry->id}}">Delete this Entry</button>

          </div>

        </div>
      </div>
    </div>

    <!-- Edit entry -->
    <div class="modal fade" id="editModal{{$entry->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="editModalLabel">Edit entry for {{ $entry->ork_name}}</h4>
          </div>
          <div class="modal-body">
            {{ Form::open(array('url' => '/entries/edit', 'class'=>'form-horizontal')) }}

            <input type="hidden" id="id" name="id" value="{{$entry->id}}">
            <input type="hidden" id="sheet_id" name="sheet_id" value="{{$id}}">



            <div class="row">
              <div class="col-sm-6 col-xs-8" style="margin-top:20px;">
                {{ Form::label('charName', 'Character (ORK) Name:') }}
                <input class="form-control" type="text" id="charName" name="charName" value="{{$entry->ork_name}}">
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6 col-xs-8" style="margin-top:20px;">
                <label for="lastName">Real Name: </label>
                <input class="form-control" type="text" placeholder="Real Name" id="realName" name="realName" value="{{$entry->real_name}}">
              </div>
            </div>

            <div class="row">
              <div class="col-sm-2 col-xs-8" style="margin-top:20px;">
                <label for="credit">Credit: </label>
                {{ Form::select('credit', $creditArray, $entry->credit) }}
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6 col-xs-8" style="margin-top:20px;">
                <label for="parkName">Park Name: </label>
                <input class="form-control" type="text" placeholder="Park Name" id="parkName" name="parkName" value="{{$entry->park}}">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Edit', array('class' => 'btn btn-info')) }}
            {{ Form::close() }}

          </div>
        </div>
      </div>
    </div>


    <!-- delete entry -->
    <div class="modal fade" id="deleteModal{{$entry->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="editModalLabel">Delete entry for {{ $entry->ork_name}}?</h4>
          </div>
          <div class="modal-body">
            {{ Form::open(array('url' => '/entries/delete', 'class'=>'form-horizontal')) }}

            <input type="hidden" id="id" name="id" value="{{$entry->id}}">
            <input type="hidden" id="sheet_id" name="sheet_id" value="{{$id}}">


            <div class="row">
              <div class="col-sm-4 col-sm-offset-2 col-xs-6">
                <strong>ORK Name:</strong> {{ $entry->ork_name}}
              </div>

              <div class="col-sm-6 col-xs-6">
                <strong>Real Name:</strong> {{ $entry->real_name }}
              </div>
            </div>

            <div class="row" style="margin-top:20px">
              <div class="col-sm-4 col-sm-offset-2 col-xs-6">
                <strong>Credit:</strong> {{ $entry->credit }}
              </div>

              <div class="col-sm-6 col-xs-6">
                <strong>Park:</strong> {{ $entry->park }}
              </div>
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  @endforeach
  @endif
</div>

@endforeach

@stop
