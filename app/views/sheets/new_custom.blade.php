@extends('layouts.base')
<?php $title="Custom Sheet" ?>

@section('content')
<div class="container">


<div class="row">
  @if (is_array($errors))
      <div class="alert alert-danger">
        <h5>There were errors with this information: </h5>
        <ul>
        @foreach ($errors as $error)
          <li>
            {{{ $error }}}
          </li>
        @endforeach
        </ul>
      </div>
  @endif

  @if(isset($success))
      <div class="alert alert-success">
        {{{ $success }}}
      </div>
  @endif

  <div class="col-xs-11 col-xs-offset-1">

  </div>
</div>


<div class="well" style="margin-top:15px">
  <h4>Custom sheet builder</h4>
  {{ Form::open(array('url' => '/sheets/custom', 'class'=>'form-horizontal')) }}
  <div class="row">
    <div class="col-sm-6 col-xs-8 col-xs-offset-3" style="margin-top:20px;">
      {{ Form::label('sheetName', 'Sheet Name:') }}
      <input class="form-control" type="text" placeholder="sheet name" id="sheetName" name="sheetName" value="{{{ $input['sheetName'] or '' }}}">
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6 col-xs-8 col-xs-offset-3" style="margin-top:20px;">
      <label for="credits">Credits per player: </label>
      <input class="form-control" type="text" placeholder="value" id="credits" name="credits" value="{{{ $input['credits'] or '' }}}">
    </div>
  </div>

    <!-- TODO: Drescription as textarea -->


  <div class="col-xs-offset-3" style="margin-top:20px;">
    {{ Form::submit('Create Custom Sheet', array('class' => 'btn btn-purple')) }}
  </div>
  <?php echo Form::token(); ?>
  {{ Form::close() }}
</div>

@stop
