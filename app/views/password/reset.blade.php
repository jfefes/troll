@extends('layouts.base')

@section('content')



  @if(Session::has('reset-error'))
  <div class="alert alert-danger">
    {{ Session::get('reset-error') }}
  </div>
  @endif
  <div class="container">

  <h5>Use this page to reset your password.</h5>


    <form action="{{ action('RemindersController@postReset') }}" method="POST">

      <div class="row">
          <input type="hidden" name="token" value="{{ $token }}">
        </div>

        <div class="row">
          <div class="col-xs-6" style="margin-top:20px">
            <input class="form-control" type="text" placeholder="Email Address" id="email" name="email_address" value="{{{ $input['email'] or '' }}}">
          </div>
        </div>

        <div class="row">
          <div class="col-xs-6" style="margin-top:20px">
            <input class="form-control" type="password" placeholder="New Password" id="password" name="password" value="">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6" style="margin-top:20px">
            <input class="form-control" type="password" placeholder="Confirm Password" id="password_confirmation" name="password_confirmation" value="">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6" style="margin-top:20px">
            <input type="submit" class="btn btn-primary" value="Reset Password">
          </div>
        </div>
      </div>
  </form>

</div>
@stop
