@extends('layouts.base')

@section('content')

<div class="container">


  @if(Session::has('remind-error'))
  <div class="alert alert-danger">
    {{ Session::get('remind-error') }}
  </div>
  @endif

  @if(Session::has('remind-status'))
  <div class="alert alert-info">
    {{ Session::get('remind-status') }}
  </div>
  @endif


  <h5>Forgot your password? Input your email address below.</h5>

  <form action="{{ action('RemindersController@postRemind') }}" method="POST">
      <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin-top:20px">
          <label for="email_address">Email:</label>
          <input class="form-control" type="email" placeholder="Email address" id="email_address" name="email_address" value="{{{ $input['email_address'] or '' }}}">
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin-top:20px">
          <input type="submit" class="btn btn-primary" value="Send email">
        </div>
      </div>
  </form>

</div>
@stop
