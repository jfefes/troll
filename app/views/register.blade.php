@extends('layouts.base')
<?php $title="New Account" ?>

@section('content')
<div class="container">




<div class="row">

  @if (is_array($errors))
      <div class="alert alert-danger">
        <h5>There were errors with this information: </h5>
        <ul>
        @foreach ($errors as $error)
          <li>
            {{{ $error }}}
          </li>
        @endforeach
        </ul>
      </div>
  @endif

  @if (isset($success))
      <div class="alert alert-success">
        {{{ $success }}}
      </div>
  @endif

    <div class="col-md-7 col-md-offset-3">
    <div class="alert alert-info text-center">
      
    </div>
  </div>
</div>

  <div class="well">
      {{ Form::open(array('url' => '/register', 'class'=>'form-horizontal')) }}
      <div class="row">
        <div class="col-sm-6 col-xs-6" style="margin-top:20px;">
          {{ Form::label('firstName', 'First Name:') }}
          <input class="form-control" type="text" placeholder="First Name" id="firstName" name="firstName" value="{{{ $input['firstName'] or '' }}}">
        </div>
        <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="lastName">Last Name: </label>
          <input class="form-control" type="text" placeholder="Last Name" id="lastName" name="lastName" value="{{{ $input['lastName'] or '' }}}">
        </div>
        <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="Username">Desired Username: </label>
          <input class="form-control" type="text" placeholder="Username" id="Username" name="Username" value="{{{ $input['Username'] or '' }}}">
        </div>
        <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="emailAddress">Email Address: </label>
          <input class="form-control" type="email" placeholder="Your Email" id="emailAddress" name="emailAddress" value="{{{ $input['emailAddress'] or '' }}}">
        </div>
        <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="password">Password: </label>
          <input class="form-control" type="password" placeholder="Password" id="password" name="password" value="{{{ $input['password'] or '' }}}">
        </div>
        <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="password_confirmation">Confirm Password: </label>
          <input class="form-control" type="password" placeholder="Confirm Password" id="password_confirmation" name="password_confirmation" value="{{{ $input['password_confirmation'] or '' }}}">
        </div>

        <!-- <div class="col-sm-6 col-xs-11" style="margin-top:20px;">
          <label for="regCode">Registration Code<span style="color:red">*</span>: </label>
          <input class="form-control" type="text" placeholder="Registration Code" id="regCode" name="regCode" value="{{{ $input['regCode'] or '' }}}">
        </div> -->

        <div class="col-xs-12" style="margin-top:20px;">
          {{ Form::submit('Create Account', array('class' => 'btn btn-primary')) }}
        </div>
        <?php echo Form::token(); ?>
      {{ Form::close() }}
    </div>
  </div>

  <p>
    <!--<h4><span style="color:red">*</span>What's this?</h4>
      As this app is still young, i'd like to keep registration limited to those who are willing to give feedback. Join the facebook group <a href="https://www.facebook.com/groups/1548159118795630/">here</a> to check out the current registration code!
    -->
  </p>


@stop
