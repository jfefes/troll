@extends('layouts.base')

<?php $title="Dashboard" ?>

@section('content')
<div class="container">

  @if(isset($status))
  <div class="alert alert-info col-md-7 col-md-offset-3">
    {{ $status }}
  </div>
  @endif

  @if(Session::has('sheet-deleted'))
  <div class="alert alert-info">
    {{ Session::get('sheet-deleted') }}
  </div>
  @endif

  <h2>Welcome, {{ $username }}</h2>

  @if($num_sheets > 0)
  <div class="row">
    <div class="pull-right"> <a href="/sheets/new" class="btn btn-info">Create a new sheet </a> </div>

    <div class="pull-right"> <a href="/sheets/custom" class="btn btn-purple" style="margin-right:10px">Custom sheet (beta)</a> </div>
  </div>

  <div class="row">
    <p class="text-center"> <br>It looks like you have a total of {{ $num_sheets }} sheet(s) availible. <br> </p>
  </div>

  <br>

  <div class="row" style="margin-bottom:-20px;">
    <div class="col-md-offset-3 col-md-5 col-sm-2 col-sm-offset-3 col-xs-4 col-xs-offset-3">
      <h6>Info</h6>
    </div>

    <div class="col-md-offset-0 col-md-push-0 col-sm-1 col-sm-offset-3 col-sm-push-1 col-xs-2">
      <h6>Use sheet</h6>
    </div>

    <div class="col-md-pull-1 col-sm-1 col-sm-push-1 col-xs-2">
      <h6>Delete</h6>
    </div>
  </div>

  @foreach ($sheets as $sheet)

    <div class="well" style="margin:10px;padding:5px">
      <div class="row">
        <div class="col-md-offset-2 col-md-7 col-sm-9 col-xs-7">
          <strong>Name:</strong> {{{ $sheet->sheet_name }}} <br> <br>
            <strong>Credits per player:</strong> {{{ $sheet->credits_per_user }}}
        </div>

        <div class="col-md-pull-1 col-sm-1 col-xs-2">
          {{ Form::open(array('url' => '/sheets/view', 'class'=>'form-horizontal')) }}
          <input type="hidden" name="id" value="{{{ $sheet->id }}}"/>

          {{Form::button('<i class="glyphicon glyphicon-circle-arrow-right" style="font-size:20px"></i>', array('type' => 'submit', 'class' => 'btn btn-info', 'style' => 'margin-top:10px'))}}
          {{ Form::close() }}
        </div>

        <div class="col-sm-1 col-xs-2">
          <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$sheet->id}}" style="margin-top:10px"><span class="glyphicon glyphicon-trash" style="font-size:20px"></span></button>

        </div>

      </div>
    </div>


        <!-- delete entry -->
        <div class="modal fade" id="deleteModal{{$sheet->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="editModalLabel">Delete sheet {{ $sheet->sheet_name}}?</h4>
              </div>
              <div class="modal-body">
                {{ Form::open(array('url' => '/sheets/delete', 'class'=>'form-horizontal')) }}

                <input type="hidden" id="id" name="id" value="{{$sheet->id}}"/>

                  <strong>Name:</strong> {{{ $sheet->sheet_name }}} <br>
                  <strong>Number of credits:</strong> {{{ $sheet->credits_per_user }}}

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>

  @endforeach

  @else
    <div class="well">
      <p> It looks like you don't have any sheets availible...</p>
      <a href="/sheets/new" class="btn btn-warning"> Let's change that. </a>
    </div>
  @endif

@stop
