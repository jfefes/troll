<!doctype html>

<html>

<head>
  <title>Amtgard Digital Sign-In Sheet </title>
</head>

<body>

  <h3>Name: {{ $name }}</h3>
  <h5>Park: {{ $park }} </h5>
  <h5>Email: {{ $email }} </h5>

  <br>
  <p> {{ $messageContents }} </p>

  <hr>

  <pre>
    .  .
   .|  |.
   ||  ||
   \\()//
   .={}=.
  / /`'\ \
  ` \  / '
     `'
   </pre>

  </body>
</html>
