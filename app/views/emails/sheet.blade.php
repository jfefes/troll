<!doctype html>

<html>

<head>
  <title>Amtgard Digital Sign-In Sheet </title>
</head>



    <!-- Code written by James Fefes.

                  .  .
                 .|  |.
                 ||  ||
                 \\()//
                 .={}=.
                / /`'\ \
                ` \  / '
                   `'


    These are Tingle's magic words. Do not steal them! -->


  <h3>Sheet name: {{ $sheet_name }}</h3>
  <h5>Credits per player: {{ $num_credits }} </h5>

<table cellspacing="10">
    <tr>
      <td> <strong>ORK Name</strong> </td>
      <td> <strong>Real Name</strong> </td>
      <td> <strong>Park</strong> </td>
      <td> <strong>Credit</strong> </td>
    </tr>

  @foreach ($entries as $entry)
  <tr style="margin-top:20px">
      <td> {{ $entry->ork_name}} </td>
      <td> {{ $entry->real_name }} </td>
      <td> {{ $entry->park }} </td>
      <td> {{ $entry->credit }} </td>
  </tr>


  @endforeach
</body>
</html>
