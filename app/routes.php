<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::post('/feedback', 'HomeController@reportFeedback');

//--------------------------- Register
			Route::get('/register', array('before' => array('force.ssl'),function() {
				return App::make('RegisterController')->index();
			}));

			Route::post('/register', array('before' => array('csrf', 'force.ssl'), function() {
				return App::make('RegisterController')->doRegister();
			}));

//--------------------------- User functions

			Route::get('/settings', array('before' => 'auth', function() {
				return App::make('UserController')->index();
			}));

			Route::post('/settings/username', array('before' => 'auth', function() {
				return App::make('UserController')->changeUsername();
			}));

			Route::post('/settings/email', array('before' => 'auth', function() {
				return App::make('UserController')->changeEmail();
			}));

			Route::post('/settings/password', array('before' => 'auth', function() {
				return App::make('UserController')->changePassword();
			}));

			Route::post('/settings/delete', array('before' => 'auth', function() {
				return App::make('UserController')->deleteUser();
			}));


//--------------------------- Dashboard functions

			Route::get('/dashboard', array('before' => 'auth', function() {
				return App::make('DashboardController')->index();
			}));

			Route::post('sheets/delete', 'DashboardController@delete');


//--------------------------- Login
			Route::get('/login', array('before' => array('force.ssl'), function() {
				return App::make('LoginController')->index();
			}));

			Route::post('/login', array('before' => array('csrf', 'force.ssl'), function() {
				return App::make('LoginController')->doLogin();
			}));

			Route::get('/logout', array('before' => 'auth', function() {
				return App::make('LoginController')->doLogout();
			}));

			Route::get('/password/remind', 'RemindersController@getRemind');

			Route::post('/password/remind', 'RemindersController@postRemind');


			Route::get('/password/reset/{token}', 'RemindersController@getReset');

			Route::post('/password/reset/{token}', 'RemindersController@postReset');



//--------------------------- Sheet functions

			Route::post('/sheets/delete', array('before' => 'auth', function() {
				return App::make('SheetController')->deleteSheet();
			}));

			Route::get('/sheets/new', array('before' => 'auth', function() {
				return App::make('SheetController')->index();
			}));

			Route::get('/sheets/custom', array('before' => 'auth', function() {
				return App::make('SheetController')->newCustom();
			}));

			Route::post('/sheets/custom', array('before' => 'auth', function() {
				return App::make('SheetController')->custom();
			}));

			Route::post('/sheets/create', array('before' => 'auth', function() {
				return App::make('SheetController')->create();
			}));

			Route::any('/sheets/view', array('before' => 'auth', function() {
				return App::make('SheetController')->view();
			}));

			Route::get('/sheets/print/{id}', function($id)
			{
					return App::make('SheetController')->printSheet($id);
			});


			Route::post('/sheets/addEntry', array('before' => 'auth', function() {
				return App::make('SheetController')->addEntry();
			}));

			Route::post('/entry/add', array('before' => 'auth', function() {
				return App::make('SheetController')->insert();
			}));

			Route::post('/entries/email', array('before' => 'auth', function() {
				return App::make('SheetController')->email();
			}));

			Route::post('/entries/edit', array('before' => 'auth', function() {
				return App::make('SheetController')->editEntry();
			}));

			Route::post('/entries/delete', array('before' => 'auth', function() {
				return App::make('SheetController')->deleteEntry();
			}));
